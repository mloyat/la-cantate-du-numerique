# La cantate du numérique

Marc Loyat - 2023

Maquette à écouter : https://soundcloud.com/marc-856997778/la-cantate-du-numerique-maquette

Il y a même un sitte dédié : https://techologie-cantate.web.deuxfleurs.fr/

## L'histoire

Ce morceau est une blague très sérieuse.
Je suis professionnel du numérique responsable, formateur, consultant, auditeur en écoconception et accessibilité et animateur de la Fresque du Numérique.

Dans une de mes rêveries sur les moyens de sensibilisation sur le sujet, j'ai eu l'idée totalement saugrenue de créer un morceau pour chorale qui parlerait des impacts du numérique. Eh ! Il y a bien une Fresque du Numérique, pourquoi pas une Cantate du Numérique !

J'ai commencé l'écriture pour de rire et je me suis pris au jeu. Quelques temps plus tard, le morceau est là.

C'est une blague parce que **personne** ne va être sensibilisé aujourd'hui par un morceau de chant a capela de 14 minutes.
Mais c'est une blague sérieuse parce que son contenu s'appuie sur des travaux de recherche tout à fait sérieux. Par ailleurs je souhaite, par ce geste inutile, interroger notre rapport au numérique : tout est-il vraiment utile ? Et si chanter ensemble nous permettait d'être plus en lien que d'être chacun sur son écran ? La numérisation sensée optimiser nos tâches qutotidiennes pour nous laisser plus de temps a-t-elle tenu ses promesses ? 

## Paroles

### Intro - l'émerveillement

Que d'usages mirifiques nous permet le numérique.

Des usages mirifiques !

Exceptionnels, envoûtants, époustouflants, palpitants, spectaculaires, enivrants, éclatants, ébouriffants ! Ahurissants !  
Incroyables, délicieux, remarquables, savoureux, presque fantastiques, voire magique, c'est formidable le numérique !

### Usages et excès

(soprane)  
Il me permet de communiquer ! J'envoie des mails à ma mamie qui habite à l'autre bout de la terre. 
Des mails avec des images de chatons en couleur et des smileys, c'est sympa les smileys, ça permet de dire des trucs sans mettre de mots et c'est rigolo.  
Sur instagram des chatons, sur facebook, des chatons, des smileys, des chatons… J'ai peur de manquer de nouveaux chatons… des chatons, des smileys rigolos, des petits chatons trop mignons...


(alto)  
À seize heure trente j'ai une visio pour aborder un projet au boulot, un truc important, j'ai pas tout compris mais ma cheffe m'a dit que j'aurais une RTT supplémentaire, ça tombe vraiment très bien, je suis à deux doigts de disjoncter, je ne vais pas tarder à faire un burn out, à deux doigts de me faire un bon gros burn out, burn out, un bon gros burn out, un gros burn out, un gros burn out, un gros burn out, un gros burn out, un gros burn out, un gros burn out, un gros burn out, un gros burn out.


(tenor)  
Le soir je branche ma console et je joue avec mes enfants à Fortnite, Mario Kart, à Call of Duty, je joue aussi en solo, j'ai beaucoup aimé Far Cry, j'y ai passé des heures et même le midi au boulot, je mangeais en vitesse et je jouais tranquilou, j'aime bien aussi les petits jeux du type tower defense et…


(basse)  
Avec ce supercalculateur je peux aggréger des tonnes de données qui proviennent de partout sur la terre, dans les airs, dans les mers, pour prévoir la météo à plusieurs jours. J'ai mis au point un modèle basé sur une intelligence artificielle, du deep learning…


(reprise soprane coupée par les remarques des tenor et basse)  
Sur doctolib tu peux prendre rendez-vous c'est pratique…

### La consommation électrique

Un ordinateur allumé a besoin d'électricité, les réseaux, les datacenters sont aussi branchés au secteur, au secteur.

Malgré une idée tenace, ceux qui tiennent la première place parmi les plus consommateurs ne sont pas les datacenters mais nos appareils quotidiens, ceux que nous tenons dans nos mains : objets connectés ou smartphones, ordinateurs, téléviseurs, dans nos mains.

### Le cycle de vie

Qui a fait la soupe que je mange ? Qu'y a-t-il dans la soupe que je mange ? (bis)

Cette question est désuète, tant nous savons que nos assiettes sont emplies de mets préparés par de dévoués cuisiniers.  
Mais les légumes moulinés ont d'abord été cultivés. Le travail des agriculteurs est bien aidé par les tracteurs qui consomment du carburant.

Cela veut dire que dans nos bols, il y a un fond de pétrole, de la sueur d'agriculteur, quelques milligrammes de tracteur, des nanogrammes de plein d'usine.

C'est fou c'qu'il y a dans nos cuisines !


Qui a fait la soupe que je mange ? Qu'y a-t-il dans la soupe que je mange ? 

Cette question qui tout à l'heure nous paraissait bien amateure, lorsqu'elle se trouve approfondie s'étend sur le cycle de vie.  
La pensée en cycle de vie.


(soprane)  
Quand j'utilise mon smartphone, mon aspirateur ou mon drone, je peux m'interroger sur ce qui a précédé et sur ce que vont devenir ces merveilles à l'avenir…  et sur ce que vont devenir ces merveilles à l'avenir.

(tenor et basse)  
J'utilise mon smartphone, mon aspirateur et mon drone. C'est le Père Noël qui a fait bosser ses lutins. Tout cela tombe du ciel et n'a pas de lendemain.


### Déchets

Incinérés ou enterrés, enterrés…

(basse)  
Où finissent nos beaux jouets, je crois qu'ils sont recyclés. Je l'ai vu affiché dans le magasin où j'ai acheté mon téléphone. Une émission à la télé m'en a également parlé.


(alto)  
Ce qui part au recyclage est de l'ordre du pourcentage. Une grande partie est dans nos tiroirs attendant un hypothétique jour de gloire, abandonnés, jamais donnés, abandonnés.


(tenor)  
Pour peu qu'ils finissent dans la bonne poubelle, ces "déchets", qui bien souvent sont fonctionnels, sont alors préparés pour être recyclés. Ou plutôt, trop souvent, décyclés.  
Mais la majorité finit incinérée ou enterrée, enterrée.


(soprane)  
Mais où sont les millions de tonnes d'ordinateurs, d'écrans ou de téléphones qui ne sont pas collectés par un organisme agréé ?  
Des organisations mafieuses les acheminent dans des pays peu regardants.  
Ils finissent brûlés à ciel ouvert au nez des enfants, des femmes et des hommes intoxiqués, empoisonnés, enterrés.

### Fabrication

Merveilles de technologie !  
Gloire à nos ingénieurs qui ont conçu les appareils qui chaque jours tombent du ciel.


Terres brûlées au fiel des produits chimiques, pourries les chairs, mercure, arsenic… cyanure… boues toxiques… infusent lentement les nappes phréatiques.


D'une tonne de roche réduite en farine seront extraits quelques grammes de métal.  
Le reste finira, amalgame toxique, derrière des digues à ciel ouvert. 

Les digues incertaines, qui trop souvent se rompent, ne sont pas, quelle veine, au fond des océans !  
(Pour le moment)

### Épuisement des ressources abiotiques

(soprane)  
Les terres rares sont moins rares que bien d'autres métaux dont il faut une cinquantaine pour faire un smartphone.  
Plus ils sont rares, plus on a besoin d'énergie pour les extraire et les traiter.


(tenor)  
Les terres rares, les métaux rares, tout est rare ici bas.  
Plus on les exploite, plus ils sont rares et plus c'est coûteux de les exploiter.


(basse)  
Rare, rare, les métaux sont rares. (bis)
Plus ils sont rares et plus on en a besoin.


Et s'il n'y a plus ces métaux, il n'y a plus de numérique.

### Wirth !

(soprane)  
Nos processeurs vont de plus en plus vite et plus ils vont vite, plus ça va très vite.  
Plus ça va très vite, plus on leur donne à faire des trucs qui nécessitent des processeurs qui vont vite.  
Du coup plus ça va vite, moins ça va très vite, moins ça va très vite, plus ça va moins vite.  
Laï lo laï...  
Nos processeurs vont de plus en plus vite et plus ils vont vite, plus va très vite.  
Plus ça va vite et moins ça va très vite et moins ça va très vite, plus on se plaint !


(alto, tenor, basse)  
Vite ! Vite! Vite ! Vite ! Tout va très vite ! Vite ! De plus en plus vite! Vite ! Tout va très vite ! (bis)  
Ploum ploum…  
Vite ! Vite! Vite ! Vite ! Ça n'va pas assez vite ! Vite ! De moins en moins vite ! Vite ! Alors on se plaint…  

### Épilogue

Il est dit qu'il nous reste une à trois générations de numérique tel que nous le connaissons.  
Et la question est de savoir si l'on préfère continuer d'accélérer ou ralentir  
pour permettre à deux ou trois générations de plus d'apprendre à se passer de numérique.
